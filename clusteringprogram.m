filename=input('please select preprocessed CSV: ','s');
csvContent=csvread(filename);

%%rng default; % For reproducibility
%%csvContent=rand(20000,3);
numClust=input('Input clusters maximum number: ');

T=clusterdata(csvContent,'maxclust',numClust);

scatter3(csvContent(:,1),csvContent(:,2),csvContent(:,3),10,T);